#!/bin/bash

###########################################################################
########################## --------------------- ##########################
########################## ----- Functions ----- ##########################
########################## --------------------- ##########################
###########################################################################

###################		Opção -l:
##### -- l -- #####			lista os ficheiros que necessitam de ser copiados, não sendo efetuada a cópia
###################			recebe 2 argumentos, um para cada directoria.

opcao_l () {

	for f in $1/*
	do
		fic=$(basename $f)
		if [ -f $1/$fic ]
		then
			if [ $1/$fic -nt $2/$fic ]
			then
				echo $fic
			fi
		fi
	done
}

###################		Opção -b:
##### -- b -- #####			(modo backup) para cada ficheiro da dir1 que vai ser copiado, salvaguarda a sua versão da dir2 concatenando ao nome do ficheiro a data e a hora da sua última alteração
###################			recebe 2 argumentos, um para cada directoria

opcao_b () {
	for f in $1/*
	do
		fic=$(basename $f)
		if [ -f $1/$fic ]
		then
			if [ $1/$fic -nt $2/$fic ]
			then
				betadate=`stat -c %y $2/$fic`
				date=${betadate:0:10}
				hour=${betadate:11:8}
				extention=${fic##*.}
				filename=${fic%.$extention}
				mv $2/$fic $2/$filename.$date.$hour.$extention
				cp -p $1/$fic $2
			fi
		fi
	done
}

###################		Opção -d:
##### -- d -- #####			(modo backup+delete) idêntica à opção -b mas mantém no máximo 4 versões antigas de cada ficheiro
###################			recebe 2 argumentos, um para cada directoria 

opcao_d () {
	for f in $1/*
	do
		fic=$(basename $f)
		if [ -f $1/$fic ]
		then
			if [ $1/$fic -nt $2/$fic ]
			then
				betadate=`stat -c %y $2/$fic`
				date=${betadate:0:10}
				hour=${betadate:11:8}
				extention=${fic##*.}
				filename=${fic%.$extention}
				apaga_antigos $2 $fic $filename $extention
				mv $2/$fic $2/$filename.$date.$hour.$extention
				cp -p $1/$fic $2
			fi
		fi
	done
}

########################		Funçao auxiliar para a função opcao_d:
## -- d (ausiliar) -- ##			recebe 3 argumentos
########################

apaga_antigos () {
	count=0
	older=$1/$2
	for file in $1/*
		do													##############
		fic=$(basename $file)				#####		 #####
		if [[ $fic == $3* ]] && *$4 ### pseudo ###
		then												#####		 #####
			count=`expr $count + 1`		##############
			if [ $older -nt $fic ]
			then
				local older=$fic
			fi
		fi
	done
	echo $count
	if [ $count -ge 4 ]
	then
		rm $1/$older
		apaga_antigos $1 $2 $3
	fi
}

###################
##### -- v -- #####
###################

opcao_v () {
  if [ $# -eq 0 ]
    cenas
  else
    cenas $1
  fi
}

###################
##### -- i -- #####
###################

#opcao_i () {
#  if [ $# -eq 0 ]
#    cenas
#  else
#    cenas $1
#  fi
#}

###################
##### -- r -- #####
###################

#opcao_r () {
#  if [ $# -eq 0 ]
#    cenas
#  elif [ $# -eq 1 ]
#    if [ "$1" != "-l" -o "$1" != "-v" -o "$1" != "-i" -o "$1" != "-b" -o "$1" != "-d" ]
#      erro
#    else
#      cenas $1
#    fi
#  else
#    if [ "$1" != "-v" -o "$1" != "-i" ]
#      erro
#    elif [ "$1" != "-b" -o "$1" != "-d" ]
#      erro
#    else
#      something something $1 $2
#    fi
#  fi
#}


##### ----- Program ----- #####

if [ "$1" = "--help" ]
  then
  echo "help"
  
elif [ "$1" = "--version" ]
  then
  echo "1.2"
  
elif [ $# -lt 2 ]
  then echo "erro1"
  
elif [ ! -d ${*: -1} ] || [ ! -d ${*: -2:1} ] # se não forem diretoria, dá erro
  then
  echo "erro"
  
else
  if [ $# -eq 2 ]
  then
		for f in $1/*
		do
			echo $f
			fic=$(basename $f)
			echo $fic
			if [ -f $fic ]
			then
				echo $fic
		    cp -p -u $fic $2
			fi
		done
    
#    dir1 = `find / -type d -name '$1' 2> /dev/null`
#    dir2 = `find / -type d -name '$2' 2> /dev/null`
    
  elif [ $# -eq 3 ]
    then
    str="$1" # torna em string
    
    if [[ $str == --* ]] # vê se começa com '--'
      then
      if [ $str == "--help"]
        then
        echo "help"
        
      elif [ $str == "--version" ]
         then
         echo "1.1"
	       
      else
        echo "erro2"
        
      fi
      
    elif [[ $str == -* ]] # vê se começa com '-'
      then
      if [ ${#str} -eq 2 ] # vê se o length da string é 2
        then
        if [ $str == "-l" ]
          then opcao_l $2 $3
          
        elif [ $str == "-b" ]
          then opcao_b $2 $3
          
        elif [ $str == "-d" ]
          then opcao_d $2 $3
          
        elif [ $str == "-v" ]
          then opcao_v
          
        elif [ $str == "-i" ]
          then opcao_i
          
        elif [ $str == "-r" ]
          then opcao_r
          
        else
          echo "erro3"
          
        fi
      
      elif [ ${#str} -gt 2 ] # vê se o length da string é maior que 2
        then
        st_command=${str:0:2} # cria uma srting, de nome st_command, que só contem os dois primeiros caracteres, isto é, o primeiro comando
        substr=${str:2} # cria uma srting, de nome substr, que contem a string menos os dois primeiros caracteres, isto é, o segundo (ou segundo e terceiro) comando(s)
        
        if [ $st_command = "-r" ]
          then
          if [[ $substr == -* && ${#substr} -eq 2 || ${#substr} -eq 1 ]]
            then
            if [ $substr = "-l" -o $substr = "l"]
              then opcao_r -
              
            elif [ $substr = "-b" -o $substr = "b"]
              then opcao_r b
              
            elif [ $substr = "-d" -o $substr = "d"]
              then opcao_r d
              
            elif [ $substr = "-v" -o $substr = "v"]
              then opcao_r v
              
            elif [ $substr = "-i" -o $substr = "i"]
              then opcao_r i
              
            else
              echo "erro4"
              
            fi
              
          elif [[ $substr == -* && ${#substr} -eq 3 ]]
            then
            nd_command=${str:0:2}
            rd_command=${str:2}
            
            if [ $nd_command = "-v" ]
              then
              if [ $rd_command = "b" ]
                then opcao_r v b
              elif [ $rd_command = "d" ]
                then opcao_r v d
              else echo "erro5"
              fi
              
            elif [ $nd_command = "-i" ]
              then
              if [ $rd_command = "b" ]
                then opcao_r i b
              elif [ $rd_command = "d" ]
                then opcao_r i d
              else echo "erro6"
              fi
              
            else echo "erro7"
            fi
          
          elif [ $substr == -* ] -a len = 4]
            then
            nd_command=${str:0:2}
            rd_command=${str:2}
            if [ $rd_command != -* ]
              then echo "erro8"
            else
              if [ $nd_command = "-v" ]
                then
                if [ $rd_command = "-b" ]
                  then opcao_r v b
                elif [ $rd_command = "-d" ]
                  then opcao_r v d
                else echo "erro9"
                fi
                
              elif [ $nd_command = "-i" ]
                then
                if [ $rd_command = "-b" ]
                  then opcao_r i b
                elif [ $rd_command = "-d" ]
                  then opcao_r i d
                else echo "erro10"
                fi
                
              else echo "erro11"
              fi
            fi
          
          elif [ ${#str} -eq 2 ]
            then
            nd_command=${str:0:1}
            rd_command=${str:1}
            
            if [ $nd_command = "v" ]
              then
              if [ $rd_command = "b" ]
                then opcao_r v b
              elif [ $rd_command = "d" ]
                then opcao_r v d
              else echo "erro12"
              fi
              
            elif [ $nd_command = "i" ]
              then
              if [ $rd_command = "b" ]
                then opcao_r i b
              elif [ $rd_command = "d" ]
                then opcao_r i d
              else echo "erro13"
              fi
              
            else echo "erro14"
            fi
          
          else echo "erro15"
          fi  

        elif [ $st_command = "-v" ]
          then
          if [[ $substr = "-b" || $substr = "b" ]]
            then opcao_v b
          elif [[ $substr = "-d" || $substr = "d" ]]
            then opcao_v d
          else echo "erro16"
          fi

        elif [ $st_command = "-i" ]
        then
          if [[ $substr = "-b" || $substr = "b" ]]
            then opcao_i b
          elif [[ $substr = "-d" || $substr = "d" ]]
            then opcao_i d
          else echo "erro17"
          fi
          
        else echo "erro18"
        fi
      fi
      
    else echo "erro19"
    fi
    
  elif [ $# -eq 4 ]
    then
    st_command="$1"
    nd_command="$2"
    
    if [ $st_command = "-r" ]
      then
      case $nd_command in
        -l) opcao_r l ;;
        
        -b) opcao_r b ;;
        
        -d) opcao_r d ;;
        
        -v) opcao_r v ;;
        
        -i) opcao_r i ;;
        
        *) echo "Erro no segundo argumento" ;;
        
      esac
    
    elif [ $st_command = "-v" ]
      then
      case $nd_command in
        -b) opcao_v b ;;
        
        -d) opcao_v d ;;
        
        *) echo "Erro no segundo argumento" ;;
        
      esac
      
    elif [ $st_command = "-i" ]
      then
      case $nd_command in
        -b) opcao_v b ;;
        
        -d) opcao_v d ;;
        
        *) echo "Erro no segundo argumento" ;;
      
      esac
      
    else echo "erro20"
    fi
    
  elif [ $# -eq 5 ]
    then
    st_command="$1"
    nd_command="$2"
    rd_command="$3"
    
    if [ $st_command = "-r" ]
      then
      case $nd_command in
        -l) opcao_r l ;;
        
        -b) opcao_r b ;;
        
        -d) opcao_r d ;;
        
        -v) case $rd_command in
              -b) opcao_r v b ;;
              
              -d) opcao_r v d ;;
            
              *) echo "Erro no terceiro argumento";;
            esac
            ;;
            
        -i) case $rd_command in
              -b) opcao_r i b ;;
              
              -d) opcao_r i d ;;
            
              *) echo "Erro no terceiro argumento";;
            esac
            ;;
        
        *) echo "Erro no segundo argumento" ;;
        
      esac

    else echo "erro21"
    fi

  else echo "erro22"  
  fi
fi
