# syncdir
### Grade - 15.1
O comando syncdir permite sincronizar duas diretorias.

#### NOME
syncdir – sincroniza duas directorias

#### SINOPSE
```bash
syncdir [OPÇÕES] dir1 dir2
```

#### OPÇÕES
--help: apresenta uma descrição das opções básicas do comando  
--version: indica a versão do comando  
-l: lista os ficheiros que necessitam de ser copiados, não sendo efetuada a cópia  
-v: (modo verbose) para cada ficheiro da dir1 informa o utilizador se a sua cópia vai ser executada ou não (e copia)  
-i: (modo interativo) para cada ficheiro da dir1 que precisa de ser copiado, pergunta ao utilizador se pretende de facto copiá-lo  
-b: (modo backup) para cada ficheiro da dir1 que vai ser copiado, salvaguarda a sua versão da dir2 concatenando ao nome do ficheiro a data e a hora da sua última alteração  
-r: (modo recursivo) executa o comando também nas subdiretorias das diretorias dadas pelo utilizador  
-d: (modo backup+delete) idêntica à opção -b mas mantém no máximo 4 versões antigas de cada ficheiro

A opção -r pode ser usada com as opções -l, -v, -i, -b e -d.  
As opções -v e -i podem ser utilizadas com as opções -b e -d.